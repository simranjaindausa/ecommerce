import 'package:ecommerce/DataBase/databaseProvider.dart';
import 'package:flutter/foundation.dart';


class ProductModel {
  late int status;
  late String message;
  late int totalRecord;
  late int totalPage;
  late List<Data> data;

  ProductModel({required this.status, required this.message, required this.totalRecord, required this.totalPage, required this.data});

  ProductModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    totalRecord = json['totalRecord'];
    totalPage = json['totalPage'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    data['totalRecord'] = this.totalRecord;
    data['totalPage'] = this.totalPage;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  late int id;
  late String slug;
  late String title;
  late String description;
  late int price;
  late String featuredImage;
  late String status;
  late String createdAt;

  Data(
      {required this.id,
        required this.slug,
        required  this.title,
        required this.description,
        required this.price,
        required this.featuredImage,
        required this.status,
        required this.createdAt});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    slug = json['slug'];
    title = json['title'];
    description = json['description'];
    price = json['price'];
    featuredImage = json['featured_image'];
    status = json['status'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['slug'] = this.slug;
    data['title'] = this.title;
    data['description'] = this.description;
    data['price'] = this.price;
    data['featured_image'] = this.featuredImage;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    return data;
  }
  Map<String,dynamic> toJSON(){
    return {
      'id': id,
      'title': title,
      'slug':slug,
      'description': description,
      'price':price,
      'featured_image':featuredImage,
      'status':status,
      'created_at':createdAt,
    };
  }

}
