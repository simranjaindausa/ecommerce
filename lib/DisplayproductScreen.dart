import 'package:ecommerce/DispalyCartScreen.dart';
import 'package:ecommerce/bloc/product_bloc.dart';
import 'package:ecommerce/bloc/product_event.dart';
import 'package:ecommerce/bloc/cart_bloc.dart';
import 'package:ecommerce/bloc/cart_event.dart';
import 'package:ecommerce/repository/product_repository.dart';
import 'package:ecommerce/repository/databseRepository.dart';
import 'package:ecommerce/widgets/ProductBody.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DisplayProductScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.blue,
          title: Text('Shopping Mall'),
          centerTitle: true,

          actions: [
            SizedBox(width: 15,),
            GestureDetector(
                onTap: () { Navigator.pushNamed(context, '/cart');
                },
                child: Icon(Icons.shopping_cart, color: Colors.white,)),
            SizedBox(width: 15,),

          ],
        ),

        body: ProductBody(),

      );
  }
}