import 'package:ecommerce/DispalyCartScreen.dart';
import 'package:ecommerce/DisplayproductScreen.dart';
import 'package:ecommerce/bloc/product_bloc.dart';
import 'package:ecommerce/bloc/product_event.dart';
import 'package:ecommerce/bloc/cart_bloc.dart';
import 'package:ecommerce/bloc/cart_event.dart';
import 'package:ecommerce/repository/product_repository.dart';
import 'package:ecommerce/repository/databseRepository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  Bloc.observer = ProductBlocObserver();
  runApp(ProductApp());

}

class ProductApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return
      MultiBlocProvider(
        providers: [
          BlocProvider(
          create: (context) => ProductBloc(
      productRepository: ProductRepository(),
    )..add(ProductFetchEvent()),),

    BlocProvider(
    create: (context) => cartBloc(
    databaseRepository: DatabseRepository(),
    )..add(CartFetchEvent()),)
    ],
     child: MaterialApp(
      title: 'Ecommerce',
      theme: ThemeData.light(),
      initialRoute: '/',
      routes: {
        '/': (context) => DisplayProductScreen(),
        '/cart':(context)=> DisplayCartScreen()
      },
    ));
  }
}

class ProductBlocObserver extends BlocObserver {
  @override
  void onEvent(Bloc bloc, Object? event) {
    print(event);
    super.onEvent(bloc, event);
  }

  @override
  void onChange(BlocBase bloc, Change change) {
    print(change);
    super.onChange(bloc, change);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    print(transition);
    super.onTransition(bloc, transition);
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    print('$error, $stackTrace');
    super.onError(bloc, error, stackTrace);
  }
}