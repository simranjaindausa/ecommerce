import 'package:ecommerce/bloc/product_bloc.dart';
import 'package:ecommerce/bloc/cart_bloc.dart';
import 'package:ecommerce/bloc/cart_state.dart';
import 'package:ecommerce/models/product_model.dart';
import 'package:ecommerce/widgets/CartListItem.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CartBody extends StatefulWidget {
  @override
  _CartBodyState createState() => _CartBodyState();
}

class _CartBodyState extends State<CartBody> {
  final List<Data> cartlist = [];
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BlocConsumer<cartBloc, CartState>(
        listener: (context, CartState) {
          if (CartState is CartLoadingState) {
            cartlist.clear();

          } else if (CartState is CartSuccessState && CartState.cartlist.isEmpty) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text('No data found')));
          } else if (CartState is CartErrorState) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(CartState.error)));
            BlocProvider.of<ProductBloc>(context).isFetching = false;
          }

          return;
        },
        builder: (context, CartState) {
          if (CartState is CartInitialState ||
              CartState is CartLoadingState && cartlist.isEmpty) {
            return CircularProgressIndicator();
          } else if (CartState is CartSuccessState) {
            cartlist.clear();
            cartlist.addAll(CartState.cartlist);
            BlocProvider.of<cartBloc>(context).isFetching = false;
            ScaffoldMessenger.of(context).hideCurrentSnackBar();
          } else if (CartState is CartErrorState && cartlist.isEmpty) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [

                const SizedBox(height: 15),
                Text(CartState.error, textAlign: TextAlign.center),
              ],
            );
          }
          return ListView.separated(

            itemBuilder: (context, index) => CartListItem(cartlist[index]),
            separatorBuilder: (context, index) => const SizedBox(height: 20),
            itemCount: cartlist.length,
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}