import 'package:ecommerce/bloc/cart_bloc.dart';
import 'package:ecommerce/bloc/cart_event.dart';
import 'package:ecommerce/bloc/cart_state.dart';
import 'package:ecommerce/models/product_model.dart';
import 'package:ecommerce/repository/databseRepository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CartListItem extends StatelessWidget {
  final Data data;


  CartListItem(this.data);
  final DatabseRepository repository = DatabseRepository();


  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(0.0),
      child: Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8.0))),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,

            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8.0),
                  bottomLeft: Radius.circular(8.0),
                ),
                child: Image.network(
                    data.featuredImage,
                    width: 150,
                    height: 150,
                    fit: BoxFit.fill

                ),
              ),
              Container(
                margin: EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    
                Text(data.title,  maxLines: 1,
           overflow: TextOverflow.ellipsis,
           softWrap: false,
                  style: TextStyle(fontSize: 18),


        ),
                    SizedBox(height: 10,),
                    Container(
                      child:
                        Text('Price',

                          style: TextStyle(fontSize: 18),


                        ),



                    ),
                    SizedBox(height: 10,),

                    Container(
                      child:
                          Text('Quantity',

                            style: TextStyle(fontSize: 18),


                          ),


                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [

                    Text(""),
                    SizedBox(height: 10,),
                    Container(
                      child:

                          Text('\$${data.price}',


                            style: TextStyle(fontSize: 18),


                          ),

                    ),
                    SizedBox(height: 10,),

                    Container(
                      child:

                          Text('1',


                            style: TextStyle(fontSize: 18),


                          ),

                    ),

                    Deletecart( item: data,)
                  ],
                ),
              )


            ],
          ),

      ),);
  }
}
class Deletecart extends StatelessWidget {
  const Deletecart({Key? key, required this.item}) : super(key: key);

  final Data item;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<cartBloc, CartState>(
      builder: (context, state) {
        if (state is CartLoadingState) {
          return const CircularProgressIndicator();
        }
        if (state is CartSuccessState) {
          return TextButton(
              onPressed:

                  () {
                    ScaffoldMessenger.of(context)
                        .showSnackBar(SnackBar(content: Text("Deleted Succesfully")));
                context.read<cartBloc>().add(CartDeleteEvent(item));
              },
              child:
              const Icon(Icons.delete, semanticLabel: 'ADDED',size: 30,)
          );
        }
        return TextButton(
            onPressed:

                () {
              context.read<cartBloc>().add(CartUpdateEvent(item));
            },
            child:
            // isInCart ?
            const Icon(Icons.delete, semanticLabel: 'ADDED',size: 30,)
          //  : const Text('ADD'),
        );

      },
    );
  }


}
