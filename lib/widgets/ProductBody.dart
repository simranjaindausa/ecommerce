import 'package:ecommerce/bloc/product_bloc.dart';
import 'package:ecommerce/bloc/product_event.dart';
import 'package:ecommerce/bloc/product_state.dart';
import 'package:ecommerce/models/product_model.dart';
import 'package:ecommerce/widgets/product_list_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProductBody extends StatefulWidget {
  @override
  _ProductBodyState createState() => _ProductBodyState();
}

class _ProductBodyState extends State<ProductBody> {
  final List<Data> _products = [];
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BlocConsumer<ProductBloc, ProductState>(
        listener: (context, productState) {
          if (productState is ProductLoadingState) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(productState.message)));
          } else if (productState is ProductSuccessState && productState.data.isEmpty) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text('No data')));
          } else if (productState is ProductErrorState) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(productState.error)));
            BlocProvider.of<ProductBloc>(context).isFetching = false;
          }
          return;
        },
        builder: (context, productState) {
          if (productState is ProductInitialState ||
              productState is ProductLoadingState && _products.isEmpty) {
            return CircularProgressIndicator();
          } else if (productState is ProductSuccessState) {
            _products.addAll(productState.data);
            BlocProvider.of<ProductBloc>(context).isFetching = false;
            ScaffoldMessenger.of(context).hideCurrentSnackBar();
          } else if (productState is ProductErrorState && _products.isEmpty) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                  onPressed: () {
                    BlocProvider.of<ProductBloc>(context)
                      ..isFetching = true
                      ..add(ProductFetchEvent());
                  },
                  icon: Icon(Icons.refresh),
                ),
                const SizedBox(height: 15),
                Text(productState.error, textAlign: TextAlign.center),
              ],
            );
          }
          return GridView.builder(
            controller: _scrollController
              ..addListener(() {
                if (_scrollController.offset ==
                    _scrollController.position.maxScrollExtent &&
                    !BlocProvider.of<ProductBloc>(context).isFetching) {
                  BlocProvider.of<ProductBloc>(context)
                    ..isFetching = true
                    ..add(ProductFetchEvent());
                }
              }),
            itemBuilder: (context, index) => ProdductListItem(_products[index]),
            itemCount: _products.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount:2),
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}