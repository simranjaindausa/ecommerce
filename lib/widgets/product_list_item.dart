import 'package:ecommerce/bloc/cart_bloc.dart';
import 'package:ecommerce/bloc/cart_event.dart';
import 'package:ecommerce/bloc/cart_state.dart';
import 'package:ecommerce/models/product_model.dart';
import 'package:ecommerce/repository/databseRepository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProdductListItem extends StatelessWidget {
  final Data data;


  ProdductListItem(this.data);
  final DatabseRepository repository = DatabseRepository();


  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8.0),

      child: Card(

        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8.0))),
        child:
           Column(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8.0),
                  topRight: Radius.circular(8.0),
                ),
                child: Image.network(
                    data.featuredImage,
                    // width: 300,
                    height: 140,
                    fit: BoxFit.fill

                ),
              ),
              SizedBox(height: 10,),

              Container(
               child: Row(children: [
                 SizedBox(width: 5,),


                 Flexible(
                   child: Text(data.title,  maxLines: 2,
                     overflow: TextOverflow.ellipsis,



                     ),
                 ),

                 new Spacer(), // I just added one line
    Container(
      child: BlocBuilder<cartBloc, CartState>(

      builder: (context, state) {
      if (state is CartLoadingState) {
      return const CircularProgressIndicator();
      }
      if (state is CartSuccessState) {
      return InkWell(
          onTap: () {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text("Added to cart Succesfully")));
      context.read<cartBloc>().add(CartUpdateEvent(data));
      },
      child:

      const Icon(Icons.shopping_cart,size: 30,)
      );
      }else {
        return InkWell(
            onTap:

                  () {
                    ScaffoldMessenger.of(context)
                        .showSnackBar(SnackBar(content: Text("Added to cart Succesfully")));
                context.read<cartBloc>().add(CartUpdateEvent(data));
            },
            child:
            const Icon(Icons.shopping_cart, size: 30)
        );
      }
      },
      ),
    ) ,//

               ],),
             )
            ],
          ),
        ),
      );
  }
}
class Addcart extends StatelessWidget {
  const Addcart({Key? key, required this.item}) : super(key: key);

  final Data item;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<cartBloc, CartState>(
      builder: (context, state) {
        if (state is CartLoadingState) {
          return const CircularProgressIndicator();
        }
        else if(state is CartupdateState){

        }
        if (state is CartSuccessState) {
          return TextButton(
              onPressed:

                  () {
                  context.read<cartBloc>().add(CartUpdateEvent(item));
                  },
              child:
              const Icon(Icons.shopping_cart, semanticLabel: 'ADDED',size: 30,)
          );
        }
                         return TextButton(
            onPressed:

                () {
                  context.read<cartBloc>().add(CartUpdateEvent(item));
            },
            child:
            const Icon(Icons.shopping_cart, semanticLabel: 'ADDED',size: 30,)
        );

      },
    );
  }


}