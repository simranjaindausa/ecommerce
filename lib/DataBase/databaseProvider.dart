import 'package:ecommerce/models/product_model.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';

class DatabaseProvider {

  static final DatabaseProvider dbProvider = DatabaseProvider();

  // the code below is used to create an instance of the database from our sqflite package
  Database? database;

  // the code below is used to create a getter for checking that if the database
  // is not null then returning the database else creating a new database
  Future<Database> get db async {
    if (database != null) {
      return database!;
    } else {
      database = await createDatabase();
      return database!;
    }
  }

  // the code below is used to create a method to create the database
  Future<Database> createDatabase() async {
    // the code below is used to get the location of the application document directory
    Directory docDirectory = await getApplicationDocumentsDirectory();
    // the code below is useed to get the path where our sqlite database will be located
    // by using the join method
    String path =
    join(docDirectory.path, "cart.db"); // here todo.db is the name of
    // our database

    // in the line of code below we need to use the openDatabase() method to
    // open the database and create the table using raw sql command
    var database = await openDatabase(
      path, // the path where our database is located
      version: 1,
      onCreate: (Database db, int version) async {
        await db.execute("CREATE TABLE cartTaBLE ("
            "id INTEGER PRIMARY KEY AUTOINCREMENT, "
            "slug TEXT, "
            "title TEXT, "
            "description TEXT, "
            "price INTEGER, "
            "featured_image TEXT,"
            "status TEXT, "
            "created_at TEXT"
            ")");
      },
      onUpgrade: (Database db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion) {}
      },
    );

    return database;
  }

}