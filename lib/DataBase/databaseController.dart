import 'package:ecommerce/DataBase/databaseProvider.dart';
import 'package:ecommerce/models/product_model.dart';

class DatabaseController{
  // the code below is used to create a property for accessing the database provider
  final dbClient = DatabaseProvider.dbProvider;

  // the code below is used to create a method to add a new task to our todo database
  Future<int> createCart(Data data) async {
    // the code below is used to get the access to the db getter
    final db = await dbClient.db;
    // the code below is used to insert the data to the todo table using the insert
    // method and passing the instance of the TODOModel as input
    var result = db.insert("cartTable", data.toJson()); // here todoTable is the name of
    // the table in the database
    return result;
  }

  // the code below is used to create a method for getting the list of TODO Tasks
  // present in the database
  Future<List<Data>> getAllCart({List<String>? columns}) async {
    // the code below is used to get the access to the db getter
    print("oijnjujnsj");

    final db = await dbClient.db;
    // the code below is used to query the database
    var result = await db.query("cartTable",columns: columns);
    // the code below is used to create a list to check if the result is not empty
    // then getting the data from the database else returning empty list
    List<Data> data = result.isNotEmpty ? result.map((item) => Data.fromJson(item)).toList() : [];
    print(data);
    return data;
  }

  // the code below is used to create a method to update the todoTable
  Future<int> updateCart(Data data) async {
    // the code below is used to get access to the db getter
    final db = await dbClient.db;
    // the code below is used to update the todo table

    var result = await db.insert("cartTable", data.toJSON());
    print(result);
    return result;
  }

  //the method below is used to Delete Todo records
  Future<int> deleteCart(int id) async {
    final db = await dbClient.db;
    var result = await db.delete("cartTable", where: 'id = ?', whereArgs: [id]);

    return result;
  }
}