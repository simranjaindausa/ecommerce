import 'package:ecommerce/bloc/product_bloc.dart';
import 'package:ecommerce/bloc/product_event.dart';
import 'package:ecommerce/bloc/cart_bloc.dart';
import 'package:ecommerce/bloc/cart_event.dart';
import 'package:ecommerce/bloc/cart_state.dart';

import 'package:ecommerce/repository/product_repository.dart';
import 'package:ecommerce/repository/databseRepository.dart';
import 'package:ecommerce/widgets/ProductBody.dart';
import 'package:ecommerce/widgets/cart_body.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DisplayCartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        appBar: AppBar(
          title: Text('Cart'),
          centerTitle: true,
        ),
        body:
        Column(
        children: [
        Expanded(
    child: Padding(
    padding: const EdgeInsets.all(15),
    child:         CartBody(),

    ),
    ),
    CartTotal()
    ],

    ));
  }
}
class CartTotal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final hugeStyle =
    Theme.of(context).textTheme.headline1?.copyWith(fontSize: 48);

    return Container(
      color: Colors.blue,
      height: 50,
      child: Center(

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            BlocBuilder<cartBloc, CartState>(builder: (context, state) {
              if (state is CartLoadingState) {
                return const CircularProgressIndicator();
              }
              if (state is CartSuccessState) {
                return Row(children: [
                  SizedBox(width: 5,),
                  Text('Total items:  ${state.cartlist.length}', style: TextStyle(fontSize: 20,color: Colors.white)),
                  Spacer(),
                  Text('Grand Total:  \$${state.cart}', style: TextStyle(fontSize: 20,color: Colors.white)),
                  SizedBox(width: 5,),

              ],);
              }
              return const Text("");
            }),

          ],
        ),
      ),
    );
  }
}