import 'dart:convert';


import 'package:ecommerce/bloc/product_event.dart';
import 'package:ecommerce/bloc/product_state.dart';
import 'package:ecommerce/models/product_model.dart';
import 'package:ecommerce/repository/product_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  final ProductRepository productRepository;
  int page = 1;
  bool isFetching = false;

  ProductBloc({
    required this.productRepository,
  }) : super(ProductInitialState());

  @override
  Stream<ProductState> mapEventToState(ProductEvent event) async* {
    if (event is ProductFetchEvent) {
      yield ProductLoadingState(message: 'Loading Data');
      final response = await productRepository.getProduct(page: page);

      if (response is http.Response) {
        print(response);

        if (response.statusCode == 200) {
          print(response.body);
           final products = jsonDecode(response.body) as Map<String, dynamic> ;
          print(response.body);
          ProductModel product=ProductModel.fromJson(products);


          yield ProductSuccessState(
            data: product.data,
          );
          page++;
        } else {
          yield ProductErrorState(error: response.body);
        }
        } else if (response is String) {
          yield ProductErrorState(error: response);
        }

    }
  }
}
class ResponseDataModel {
  late int statusCode;
  dynamic data;
}