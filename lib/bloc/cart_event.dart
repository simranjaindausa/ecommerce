

import 'package:ecommerce/models/product_model.dart';

abstract class CartEvent {
  const CartEvent();
}

class CartFetchEvent extends CartEvent {
  const CartFetchEvent();
  List<Object> get props => [];

}
class CartUpdateEvent extends CartEvent{
  final Data item;

  const CartUpdateEvent(this.item);
}
class CartDeleteEvent extends CartEvent{
  final Data item;

  const CartDeleteEvent(this.item);
}