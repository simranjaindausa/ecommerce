import 'dart:convert';



import 'package:ecommerce/bloc/product_state.dart';
import 'package:ecommerce/bloc/cart_event.dart';
import 'package:ecommerce/bloc/cart_state.dart';

import 'package:ecommerce/models/product_model.dart';
import 'package:ecommerce/repository/product_repository.dart';
import 'package:ecommerce/repository/databseRepository.dart';
import 'package:ecommerce/widgets/product_list_item.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';

class cartBloc extends Bloc<CartEvent, CartState> {
  final DatabseRepository databaseRepository;
  bool isFetching = false;

  cartBloc({
    required this.databaseRepository,
  }) : super(CartInitialState());

  @override
  Stream<CartState> mapEventToState(CartEvent event) async* {
    if (event is CartFetchEvent) {
      yield CartLoadingState(message: 'Loading Data');
      print("dkjbcjd");
      List<Data> cartlist = await databaseRepository.getAllCart();


      if (cartlist.length >0) {

        double totalScores = 0.0;
        // looping over data array
        cartlist.forEach((item){
          //getting the key direectly from the name of the key
          totalScores += item.price;
        });

        yield CartSuccessState(
          cartlist: cartlist,
          cart:totalScores
        );
        } else {
          yield CartErrorState(error: "Cart is Empty");
        }
      }
    if (event is CartUpdateEvent) {
      print(event.item);
      databaseRepository.updateCart(event.item);

      List<Data> cartlist = await databaseRepository.getAllCart();


      if (cartlist.length >0) {

        double totalScores = 0.0;
        // looping over data array
        cartlist.forEach((item){
          //getting the key direectly from the name of the key
          totalScores += item.price;
        });

        yield CartSuccessState(
            cartlist: cartlist,
            cart:totalScores
        );

      } else {
        yield CartErrorState(error: "Cart is Empty");
      }

    }
    if (event is CartDeleteEvent){
      print(event.item);
      databaseRepository.deleteCart(event.item.id);

      yield CartLoadingState(message: 'Loading Data');
      List<Data> cartlist = await databaseRepository.getAllCart();


      if (cartlist.length >0) {

        double totalScores = 0.0;
        // looping over data array
        cartlist.forEach((item){
          //getting the key direectly from the name of the key
          totalScores += item.price;
        });

        yield CartSuccessState(
            cartlist: cartlist,
            cart:totalScores
        );

      } else {
        yield CartErrorState(error: "Cart is Empty");
      }
    }

  }
}
