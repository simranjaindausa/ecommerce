
import 'package:ecommerce/models/product_model.dart';

abstract class CartState {
  const CartState();
}

class CartInitialState extends CartState {
  const CartInitialState();
}

class CartLoadingState extends CartState {
  final String message;

  const CartLoadingState({
    required this.message,
  });
}

class CartSuccessState extends CartState {
  final List<Data> cartlist;
  final double cart;


  const CartSuccessState({
    required this.cartlist,
    required this.cart
  });
}

class CartErrorState extends CartState {
  final String error;

  const CartErrorState({
    required this.error,
  });
}

class CartupdateState extends CartState {
  final String message;

  const CartupdateState({
    required this.message,
  });
}


