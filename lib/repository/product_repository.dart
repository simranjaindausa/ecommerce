import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class ProductRepository {
  static final ProductRepository _productRepository = ProductRepository._();
  //static const int _perPage = 10;

  ProductRepository._();

  factory ProductRepository() {
    return _productRepository;
  }

  Future<dynamic> getProduct({
    required int page,
  }) async {
    Map<String, dynamic> params =  {
      "page": page.toString(),
      "perPage":"5",

    };
    Map<String, String> Headers= {
      "Accept": "application/json",
    "Content-Type": "application/x-www-form-urlencoded",
      "token":"eyJhdWQiOiI1IiwianRpIjoiMDg4MmFiYjlmNGU1MjIyY2MyNjc4Y2FiYTQwOGY2MjU4Yzk5YTllN2ZkYzI0NWQ4NDMxMTQ4ZWMz"
    };
    try {
      return await http.post(Uri.parse('http://205.134.254.135/~mobile/MtProject/public/api/product_list.php',)
          ,body:params,headers:Headers );
    } catch (e) {
      return e.toString();
    }
  }
}