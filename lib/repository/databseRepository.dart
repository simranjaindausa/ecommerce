import 'package:ecommerce/DataBase/databaseController.dart';
import 'package:ecommerce/models/product_model.dart';

class DatabseRepository{
  // the code below is used to create an instance of the DatabaseController class
  final DatabaseController dbController = DatabaseController();

  Future getAllCart() => dbController.getAllCart();

  Future insertCart(Data data) => dbController.createCart(data);

  Future updateCart(Data data) => dbController.updateCart(data);

  Future deleteCart(int index) => dbController.deleteCart(index);
}